import os
from django.conf import settings
from django.db import models


def service_images_path():
    return os.path.join(settings.STATIC_URL, 'images')


class ServiceCategory(models.TextChoices):
    MENS_HAIR = "Men's Hair", "Men's Haircuts"
    WOMENS_HAIR = "Women's Hair", "Women's Haircuts"
    MAKEUP = 'Makeup', 'Makeup'
    MENS_BEARD = 'Beard', "Men's Beard"
    NAILS = 'Nails', 'Nails'
    EYEBROWS = 'Eyes', 'Eyebrows and Facial'


class Service(models.Model):
    name = models.CharField(max_length=80)
    description = models.CharField(max_length=500)
    key = models.CharField(max_length=20, primary_key=True)
    category = models.CharField(
        max_length=15,
        choices=ServiceCategory.choices
    )
    image_path = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Gromming Service"
        verbose_name_plural = "Gromming Services"

    def __str__(self):
        return self.name
