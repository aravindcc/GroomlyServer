from django.urls import path
from .views import ClientProfileView, ServiceDataView

urlpatterns = [
    path('profile/', ClientProfileView.as_view(), name="client_profile"),
    path('services/', ServiceDataView.as_view(), name="services"),
]
