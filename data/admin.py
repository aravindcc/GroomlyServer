from django.contrib import admin
from .models import Service


class ServiceAdmin(admin.ModelAdmin):
    model = Service


admin.site.register(Service, ServiceAdmin)
