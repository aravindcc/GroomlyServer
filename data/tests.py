from django.test import TestCase
from rest_framework.test import APIClient
from django.core.management import call_command
from django.contrib.auth.hashers import make_password
from io import StringIO

from .models import Service, ServiceCategory
from authentication.models import GroomlyUser

from bookings.tests import setup_default, format_seconds


class ServiceTestCase(TestCase):

    def setUp(self):
        self.dataDirectory = "../Data/Converted"

    def test_import_services(self):
        out = StringIO()
        call_command('import_services', self.dataDirectory, stdout=out)
        self.assertFalse('Error' in out.getvalue())
        self.assertEqual(len(Service.objects.all()), 61)


class ClientProfileTestCase(TestCase):

    def setUp(self):
        setup_default(self)

    def test_profile(self):
        # check can create profile
        description = "HELLO!"
        matrix = []
        for (i, service) in enumerate(self.services):
            matrix.append({
                "id": service.key,
                "price": i * 2 + 1,
                "duration": format_seconds(i * 60 + 900),
            })
        profile = {
            "description": description,
            "matrix": matrix,
        }
        c = APIClient()
        c.force_authenticate(user=self.client)
        response = c.post('/data/profile/', profile, format='json')
        self.assertEqual(response.status_code, 201)

        # checking auth required to get
        c.force_authenticate(user=None)
        response = c.get(f'/data/profile/?clientID={self.client.id}')
        self.assertEqual(response.status_code, 403)

        # check customer can get profile
        c.force_authenticate(user=self.customer)
        response = c.get(
            f'/data/profile/?clientID={self.client.id}', format='json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('description', response.data)
        self.assertIn('matrix', response.data)
        self.assertEqual(response.data['description'], description)
        self.assertEqual(len(response.data['matrix']), len(matrix))

        description = "new desc!"
        matrix = []
        for (i, service) in enumerate(self.services):
            if i > 5:
                break
            matrix.append({
                "id": service.key,
                "price": i * 5 + 1,
                "duration": format_seconds(i * 80 + 900),
            })
        profile = {
            "description": description,
            "matrix": matrix,
        }
        c.force_authenticate(user=self.client)
        response = c.post('/data/profile/', profile, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertIn('matrix', response.data)
        self.assertEqual(response.data['description'], description)
        self.assertEqual(len(response.data['matrix']), len(matrix))
        for given in response.data['matrix']:
            for og in matrix:
                if og["id"] == given["id"]:
                    self.assertEqual(og["price"], given["price"])
                    self.assertEqual(og["duration"], given["duration"])

    def test_invalidservice_profile(self):
        description = "HELLO!"
        matrix = [{"id": "fake", "price": 10, "duration": 10}]
        profile = {
            "description": description,
            "matrix": matrix,
        }
        c = APIClient()
        c.force_authenticate(user=self.client)
        response = c.post('/data/profile/', profile, format='json')
        self.assertEqual(response.status_code, 400)

    def test_customer_post_fails(self):
        description = "HELLO!"
        matrix = [{"id": "fake", "price": 10, "duration": 10}]
        profile = {
            "description": description,
            "matrix": matrix,
        }
        c = APIClient()
        c.force_authenticate(user=self.customer)
        response = c.post('/data/profile/', profile, format='json')
        self.assertEqual(response.status_code, 403)
