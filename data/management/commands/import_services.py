from django.core.management.base import BaseCommand, CommandError
from data.models import Service
import os.path
from os import makedirs
import json
from pathlib import Path
from shutil import copytree, rmtree


class Command(BaseCommand):
    help = 'Imports data - will delete and replace servies, make sure service_id is the same'

    def add_arguments(self, parser):
        def is_valid_file(parser, arg):
            if not os.path.exists(arg):
                parser.error("Error: The file %s does not exist!" % arg)
            else:
                return arg  # return an open file handle
        parser.add_argument("filename",
                            help="converted data directory", metavar="FILE",
                            type=lambda x: is_valid_file(parser, x))
        parser.add_argument('-static', action='store_true')

    def handle(self, *args, **options):
        dataPath = Path(options['filename'])
        jsonFile = dataPath / "data.json"
        with open(jsonFile, 'r') as file:
            data = json.loads(file.read())
            Service.objects.all().delete()
            for (category, services) in data.items():
                for (service_id, service_config) in services.items():
                    name = service_config['name']
                    description = service_config['description']
                    if description == "":
                        self.stdout.write(self.style.SUCCESS(
                            f'Error creating {name} in {category} - no description'))
                        continue
                    image_path = service_config['image']
                    new_service = Service(
                        name=name,
                        description=description,
                        key=category + str(service_id),
                        category=category,
                        image_path=image_path
                    )
                    new_service.save()
                    self.stdout.write(self.style.SUCCESS(
                        f'Successfully created {name} in {category}'))

        if options['static']:
            # set up folders
            image_folder_name = "serviceImages"
            static_path = Path(os.path.abspath(os.path.dirname(
                __name__))) / "static"
            static_image_dir = static_path / image_folder_name
            image_dir = dataPath / image_folder_name
            makedirs(static_path, exist_ok=True)

            # copy over to static folder
            if os.path.exists(static_image_dir):
                rmtree(static_image_dir)
            copytree(image_dir, static_image_dir)
            self.stdout.write(self.style.SUCCESS(
                f'Successfully moved images to static folder'))
