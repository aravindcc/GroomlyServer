from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import PermissionDenied

from .models import Service
from .serializers import ServiceSerializer
from authentication.serializers import ClientProfileSerializer
from authentication.models import GroomlyUser, ClientProfile
from authentication.custom_permissions import ClientPermission

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

get_client_id_param = openapi.Parameter(
    'clientID', openapi.IN_QUERY, description="user ID of client", type=openapi.TYPE_INTEGER)


class ServiceDataView(APIView):

    '''
        post:
        Create/Update the profile of the authenticated client
        get:
        Get the profile of a client by id.
    '''

    def get_serializer(self):
        return ServiceSerializer()

    def get(self, request):
        serializer = ServiceSerializer(Service.objects.all(), many=True)
        return Response(serializer.data)


class ClientProfileView(APIView):

    '''
        post:
        Create/Update the profile of the authenticated client
        get:
        Get the profile of a client by id.
    '''

    def get_serializer(self):
        return ClientProfileSerializer()

    def check_permissions(self, request):
        if request.method == 'GET':
            permission = permissions.IsAuthenticated()
        else:
            permission = ClientPermission()
        has_permissions = permission.has_permission(request, self)
        if not has_permissions:
            raise PermissionDenied

    def post(self, request, format='json'):
        input_data = request.data.copy()
        input_data['clientID'] = request.user.id
        try:
            profile = ClientProfile.objects.get(client=request.user)
            serializer = ClientProfileSerializer(profile, data=input_data)
        except Exception as e:
            serializer = ClientProfileSerializer(data=input_data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(manual_parameters=[get_client_id_param])
    def get(self, request):
        try:
            clientID = int(request.GET['clientID'])
            client = GroomlyUser.objects.get(id=clientID)
            if client.is_client is False or client.profile is None:
                raise Exception("INVALID USER")
            serialiser = ClientProfileSerializer(client.profile)
            return Response(serialiser.data)
        except:
            return Response("Bad request!", status=status.HTTP_400_BAD_REQUEST)
