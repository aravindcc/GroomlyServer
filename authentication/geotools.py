import geocoder
from math import pi, sin, cos, sqrt, atan2
import googlemaps

from rest_framework.serializers import ValidationError

google_api_key = "AIzaSyCT8d-yPNPaqGJ1s64u7e2UhkSp-PTTEzA"
gmaps = googlemaps.Client(key=google_api_key)


def validate_address(address_line):
    deocoded = geocoder.google(address_line, key=google_api_key)
    try:
        if not deocoded.ok:
            raise Exception()
        ne = deocoded.bbox['northeast']
        sw = deocoded.bbox['southwest']
        dist = as_crow_distance(ne, sw)
        if dist > 0.5:
            raise Exception(dist)
        return deocoded.latlng, deocoded.address
    except:
        raise ValidationError("please enter valid address")


def radian_latlng(from_user):
    lat = (pi * from_user[0]) / 180
    lng = (pi * from_user[1]) / 180
    return lat, lng


def as_crow_distance(ne, sw):
    R = 6371
    (lat1, lng1) = radian_latlng(ne)
    (lat2, lng2) = radian_latlng(sw)
    dlat = lat2 - lat1
    dlng = lng2 - lng2
    a = sin(dlat/2) * sin(dlat/2) + cos(lat1) * \
        cos(lat2) * sin(dlng/2) * sin(dlng/2)
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    d = R * c
    return d


def driving_distance(from_user, to_users):
    origin = (from_user.latitude, from_user.longitude)
    distances = []
    for user in to_users:
        destination = (user.latitude, user.longitude)
        result = driving_distance_points(origin, destination)
        distances.append(result)
    return distances


def driving_distance_points(origin, destination):
    result = gmaps.distance_matrix(
        origin, destination, mode='driving', units='imperial')
    return result["rows"][0]["elements"][0]["distance"]["text"]


def check_max_distance(distance):
    try:
        distance_val = float(distance.split()[0])
        if "mi" in distance and distance_val > 20:
            raise Exception()
    except:
        raise ValidationError('invalid location.')


def combine_and_filter_distances(distances, ys):
    if len(distances) != len(ys):
        return []
    output = {}
    for i in range(len(distances)):
        try:
            check_max_distance(distances[i])
            output[ys[i]] = distances[i]
        except:
            continue
    return output
