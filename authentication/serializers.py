from rest_framework import serializers
from .models import GroomlyUser, ClientProfile, ServicePrice, Service
from .geotools import validate_address
from datetime import timedelta


class ServicePriceSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='service.key')
    price = serializers.FloatField(min_value=0)
    duration = serializers.DurationField(
        min_value=timedelta(minutes=15), max_value=timedelta(hours=3))

    class Meta:
        model = ServicePrice
        fields = ('id', 'price', 'duration')


class ClientProfileSerializer(serializers.ModelSerializer):
    description = serializers.CharField()
    matrix = ServicePriceSerializer(source='serviceprice_set', many=True)
    clientID = serializers.IntegerField(source='client.id')

    class Meta:
        model = ClientProfile
        fields = ('clientID', 'description', 'matrix')

    def get_service_price(self, client, service):
        if service is None:
            raise serializers.ValidationError("invalid service.")
        try:
            service_price = ServicePrice.objects.get(
                client=client, service=service)
        except:
            service_price = ServicePrice(
                client=client, service=service, price=0.0)
        return service_price

    def update_services(self, profile, validated_data):
        client = profile.client
        service_data = validated_data.pop('serviceprice_set', [])
        services = {}
        for service_obj in service_data:
            try:
                key = service_obj.pop("service").pop("key")
                price = service_obj.pop("price")
                duration = service_obj.pop("duration")
                tb = Service.objects.get(key=key)
                services[tb] = (price, duration)
            except:
                raise serializers.ValidationError('invalid service.')

        current_service_prices = ServicePrice.objects.filter(client=profile)
        for c_sp in current_service_prices:
            if c_sp.service in services:
                c_sp.price, c_sp.duration = services.pop(c_sp.service)
                c_sp.save()
            else:
                c_sp.delete()
        for (service, detail) in services.items():
            obj = self.get_service_price(profile, service)
            obj.price, obj.duration = detail
            obj.save()

    def create(self, validated_data):
        try:
            client_id = validated_data.pop('client').pop('id')
            client = GroomlyUser.objects.get(id=client_id)
            if not client.is_client:
                raise serializers.ValidationError("invalid client.")
            validated_data["client"] = client
        except:
            raise serializers.ValidationError("invalid client.")
        client = validated_data["client"]
        description = validated_data.pop('description', "")
        profile = ClientProfile(client=client, description=description)
        profile.save()
        self.update_services(profile, validated_data)
        return profile

    def update(self, instance, validated_data):
        description = validated_data.pop('description', "")
        instance.description = description
        self.update_services(instance, validated_data)
        instance.save()
        return instance


class GroomlyUserSerializer(serializers.ModelSerializer):
    """
    Currently unused in preference of the below.
    """
    email = serializers.EmailField(
        required=True
    )
    password = serializers.CharField(min_length=8, write_only=True)
    address = serializers.CharField()
    city = serializers.CharField()
    postal_code = serializers.CharField()

    class Meta:
        model = GroomlyUser
        fields = ('email', 'password',
                  'address', 'city', 'postal_code')
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, data):
        """
        Check that address is valid
        """
        if GroomlyUser.objects.filter(email=data['email']).exists():
            raise serializers.ValidationError("email address already in use.")

        address_comps = [data['address'], data['city'], data['postal_code']]
        address_line = ', '.join(address_comps)
        latlng, formatted = validate_address(address_line)

        verify = 0
        crimped = address_line.replace(" ", "")
        for c in crimped:
            if c in formatted:
                verify += 1
        if (verify / len(crimped)) < 0.7:
            raise serializers.ValidationError('invalid address.')

        data['latitude'], data['longitude'] = latlng
        data['formatted_address'] = formatted
        return data

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        # as long as the fields are the same, we can just use this
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class ClientListSerialiser(serializers.ModelSerializer):
    distance = serializers.SerializerMethodField()
    profile = ClientProfileSerializer()

    class Meta:
        model = GroomlyUser
        fields = ('id', 'distance', 'profile')

    def get_distance(self, obj) -> float:
        return self.context[obj]
