from django.test import TestCase
from rest_framework.test import APIClient
from .models import GroomlyUser
from .serializers import GroomlyUserSerializer
from django.contrib.auth.hashers import make_password
import json


class UserTestCase(TestCase):
    def get_user_info(self, num):
        email = f'{num}@test.com'
        addresses = [
            'Robinson College',
            '33 Loyds Close',
            '20 Ripson Close',
            'The Brambles, St Mary Bourne',
            'Kingston Bagpuize',
            '20 Gibson Close',
        ]
        city = [
            'Cambridge',
            'Abingdon',
            'Abingdon',
            'Andover',
            'Oxford',
            'Abingdon'
        ]
        postal_code = [
            'CB39AN',
            'OX141XR',
            'GHV1XZ',
            'SP116EN',
            'OX13',
            'OX141XT',
        ]
        password = 'strongpword'
        return {
            "email": email,
            "address": addresses[num],
            "city": city[num],
            "postal_code": postal_code[num],
            "password": password
        }

    def setUp(self):
        customer_info = self.get_user_info(0)
        customer_info["password"] = make_password(customer_info["password"])
        customer_info["formatted_address"] = "Grange Rd, Cambridge CB3 9AN, UK"
        customer_info["latitude"] = 52.2049386
        customer_info["longitude"] = 0.1042334
        GroomlyUser.objects.create(**customer_info)

    def load_case(self, num, code):
        c = APIClient()
        customer_info = self.get_user_info(num)
        response = c.post('/auth/user/', customer_info, format='json')
        self.assertEqual(response.status_code, code)

    def test_duplicate_user(self):
        self.load_case(0, 400)

    def test_user_creation(self):
        self.load_case(1, 201)

    def test_invalid_address(self):
        self.load_case(2, 400)

    def test_named_address(self):
        self.load_case(3, 201)

    def test_town_address_fails(self):
        self.load_case(4, 400)

    def login_user(self, user, client):
        data = {
            'email': user.email,
            'password': 'strongpword'
        }
        response = client.post(
            "/auth/token/obtain/", data, format='json')
        self.assertEqual(response.status_code, 200,
                         "The token should be successfully returned.")
        response_content = json.loads(response.content.decode('utf-8'))
        token = response_content["access"]
        header = f'JWT {token}'
        client.credentials(HTTP_AUTHORIZATION=header)

    def test_require_login_for_get(self):
        c = APIClient()
        response = c.get('/auth/user/')
        self.assertTrue(response.status_code ==
                        400 or response.status_code == 403)

    def test_za_check_clients_appear(self):
        c = APIClient()
        self.load_case(5, 201)
        self.load_case(1, 201)
        customer = GroomlyUser.objects.get(id=2)
        customer.is_client = False
        customer.save()
        self.login_user(customer, c)
        for user in GroomlyUser.objects.all():
            if user.id != 2:
                user.is_client = True
                user.save()
        response = c.get('/auth/user/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertTrue('id' in response.data[0])
        self.assertTrue('distance' in response.data[0])
        self.assertTrue('profile' in response.data[0])

    def test_zb_check_no_client_available(self):
        c = APIClient()
        self.load_case(5, 201)
        self.load_case(1, 201)
        customer = GroomlyUser.objects.get(id=1)
        customer.is_client = False
        customer.save()
        self.login_user(customer, c)
        for user in GroomlyUser.objects.all():
            if user.id != 1:
                user.is_client = True
                user.save()
        response = c.get('/auth/user/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)
