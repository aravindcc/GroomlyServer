from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import PermissionDenied

from .serializers import GroomlyUserSerializer, ClientListSerialiser
from .models import GroomlyUser
from .geotools import driving_distance, combine_and_filter_distances

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from rest_framework_simplejwt.tokens import RefreshToken, AccessToken

# get parameters
user_response = openapi.Response(
    'List of clients in nearby area', ClientListSerialiser(many=True))


class LogoutAndBlacklistRefreshTokenForUserView(APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = ()

    def post(self, request):
        try:
            token = RefreshToken(request.data["refresh"])
            token.blacklist()
            return Response({"state": "reset"}, status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            print(str(e))
            return Response(status=status.HTTP_400_BAD_REQUEST)


class GroomlyUserView(APIView):
    '''
        post:
        Create a customer, will perform address validation.
        get:
        Get nearby clients of a customer
    '''

    permission_classes = (permissions.AllowAny,)

    def get_serializer(self):
        return GroomlyUserSerializer()

    def check_permissions(self, request):
        if request.method == 'GET':
            permission = permissions.IsAuthenticated()
        else:
            permission = permissions.AllowAny()
        has_permissions = permission.has_permission(request, self)
        if not has_permissions:
            raise PermissionDenied

    @swagger_auto_schema(security=[])
    def post(self, request, format='json'):
        serializer = GroomlyUserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(manual_parameters=[], responses={200: user_response})
    def get(self, request):
        try:
            clients = list(GroomlyUser.objects.filter(
                is_client=True).exclude(latitude__isnull=True))
            source = request.user
            if source.is_client == True:
                raise Exception()
            distances = []
            if len(clients) > 0:
                distances = driving_distance(source, clients)
            resplist = combine_and_filter_distances(distances, clients)
            serializer = ClientListSerialiser(
                list(resplist.keys()), many=True, context=resplist)
            return Response(serializer.data)
        except Exception as e:
            print(str(e))
            return Response('Bad Request!', status=status.HTTP_400_BAD_REQUEST)
