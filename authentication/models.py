from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from data.models import Service

# todo migrate client profile and service to data.


class GroomlyUserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class GroomlyUser(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    is_client = models.BooleanField(default=False)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=165)
    postal_code = models.CharField(max_length=10)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    formatted_address = models.CharField(max_length=200)

    objects = GroomlyUserManager()


class ClientProfile(models.Model):
    client = models.OneToOneField(
        GroomlyUser,
        on_delete=models.CASCADE,
        limit_choices_to={'is_client': True},
        related_name='profile',
    )
    description = models.CharField(max_length=500)
    price_matrix = models.ManyToManyField(
        Service,
        through='ServicePrice',
    )


class ServicePrice(models.Model):
    client = models.ForeignKey(
        ClientProfile, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    price = models.FloatField()
    duration = models.DurationField()
