from django.contrib import admin
from .models import GroomlyUser


class GroomlyUserAdmin(admin.ModelAdmin):
    model = GroomlyUser


admin.site.register(GroomlyUser, GroomlyUserAdmin)
