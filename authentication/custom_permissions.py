from rest_framework import permissions


class ClientPermission(permissions.BasePermission):
    """
    Global permission check for blacklisted IPs.
    """

    def has_permission(self, request, view):
        try:
            if request.user.is_client:
                return True
        except AttributeError:
            print("Not Platform User - must be admin-type user")
        return False
