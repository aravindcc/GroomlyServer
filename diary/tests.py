from django.test import TestCase
from rest_framework.test import APIClient
from django.contrib.auth.hashers import make_password

from datetime import datetime, time

from .models import Availability
from authentication.models import GroomlyUser

from bookings.tests import setup_default


class AvailabilityTestCase(TestCase):

    def setUp(self):
        setup_default(self)

    def test_create_availability(self):
        date_str = "17-5-2020"
        blocks = [
            {
                "start": "10:00",
                "end": "11:00"
            },
            {
                "start": "14:00",
                "end": "15:00"
            },
        ]
        input = {
            "date": date_str,
            "blocks": blocks,
        }
        client = APIClient()
        client.force_authenticate(user=self.client)
        response = client.post('/diary/availability/', input, format='json')
        self.assertEqual(response.status_code, 201)

        # check customer can read properly
        client.force_authenticate(user=self.customer)
        response = client.get(
            f'/diary/availability/?clientID={self.client.id}&date={date_str}')
        self.assertEqual(response.status_code, 200)
        self.assertIn("blocks", response.data)
        self.assertEqual(len(response.data["blocks"]), 2)

        blocks = [
            {
                "start": "15:00",
                "end": "16:00"
            },
        ]
        input = {
            "date": date_str,
            "blocks": blocks,
        }
        client.force_authenticate(user=self.client)
        response = client.post('/diary/availability/', input, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertIn("blocks", response.data)
        self.assertEqual(len(response.data["blocks"]), 1)
        self.assertIn("15:00", response.data["blocks"][0]["start"])

    def test_create_invalid_block(self):
        date_str = "17-5-2020"
        blocks = [{
            "start": f"11:00",
            "end": f"10:00"
        }]
        input = {
            "date": date_str,
            "blocks": blocks,
        }
        client = APIClient()
        client.force_authenticate(user=self.client)
        response = client.post('/diary/availability/', input, format='json')
        self.assertEqual(response.status_code, 400)

    def test_create_to_short_block(self):
        date_str = "17-5-2020"
        blocks = [{
            "start": f"10:00",
            "end": f"10:10"
        }]
        input = {
            "date": date_str,
            "blocks": blocks,
        }
        client = APIClient()
        client.force_authenticate(user=self.client)
        response = client.post('/diary/availability/', input, format='json')
        self.assertEqual(response.status_code, 400)

    def test_only_client_edit(self):
        client = APIClient()
        client.force_authenticate(user=self.customer)
        response = client.post('/diary/availability/', {}, format='json')
        self.assertEqual(response.status_code, 403)
