from django.contrib import admin
from .models import Availability, AvailableBlocks


class AvailabilityAdmin(admin.ModelAdmin):
    model = Availability


class AvailableBlockAdmin(admin.ModelAdmin):
    model = AvailableBlocks


admin.site.register(Availability, AvailabilityAdmin)
admin.site.register(AvailableBlocks, AvailableBlockAdmin)
