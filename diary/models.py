from django.db import models
from authentication.models import GroomlyUser


class Availability(models.Model):
    date = models.DateField()
    client = models.ForeignKey(
        GroomlyUser,
        on_delete=models.CASCADE,
        limit_choices_to={'is_client': True},
        related_name='+',
    )

    class Meta:
        verbose_name = "Availability"
        verbose_name_plural = "Availabilities"


class AvailableBlocks(models.Model):
    start = models.TimeField()
    end = models.TimeField()
    service = models.ForeignKey(
        Availability,
        on_delete=models.CASCADE,
        related_name='blocks',
    )

    class Meta:
        verbose_name = "Available Block"
        verbose_name_plural = "Available Blocks"
