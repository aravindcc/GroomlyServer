from rest_framework import serializers
from authentication.models import GroomlyUser
from .models import Availability, AvailableBlocks
from dateutil import parser

from bookings.serializers import check_min_elapsed


class AvailableBlockSerializer(serializers.ModelSerializer):
    start = serializers.TimeField()
    end = serializers.TimeField()

    class Meta:
        model = AvailableBlocks
        fields = ('start', 'end', )


class AvailableSerializer(serializers.ModelSerializer):
    date = serializers.DateField(format="%d-%m-%Y")
    blocks = AvailableBlockSerializer(many=True)
    clientID = serializers.IntegerField(source='client.id')

    class Meta:
        model = Availability
        fields = ('clientID', 'date', 'blocks', )

    def update_services(self, day_diary, validated_data):
        client = day_diary.client
        block_list = validated_data.pop('blocks', [])

        for period in block_list:
            check_min_elapsed(day_diary.date, period["start"], period["end"])

        current_service_prices = AvailableBlocks.objects.filter(
            service=day_diary)
        for c_sp in current_service_prices:
            if len(block_list) > 0:
                period = block_list.pop()
                c_sp.start = period["start"]
                c_sp.end = period["end"]
                c_sp.save()
            else:
                c_sp.delete()
        for period in block_list:
            obj = AvailableBlocks(
                start=period["start"], end=period["end"], service=day_diary)
            obj.save()

    def create(self, validated_data):
        try:
            client_id = validated_data.pop('client').pop('id')
            client = GroomlyUser.objects.get(id=client_id)
            if not client.is_client:
                raise serializers.ValidationError("invalid client.")
            validated_data["client"] = client
        except:
            raise serializers.ValidationError("invalid client.")
        client = validated_data["client"]
        date = validated_data["date"]
        profile = Availability(client=client, date=date)
        profile.save()
        self.update_services(profile, validated_data)
        return profile

    def update(self, instance, validated_data):
        self.update_services(instance, validated_data)
        instance.save()
        return instance
