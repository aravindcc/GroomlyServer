from django.urls import path
from .views import AvailabilityView, DiaryView

urlpatterns = [
    path('', DiaryView.as_view(), name="diary_view"),
    path('availability/', AvailabilityView.as_view(), name="availability_view"),
]
