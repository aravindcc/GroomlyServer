from django.shortcuts import render
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import PermissionDenied

from .serializers import AvailableSerializer
from .models import Availability, AvailableBlocks
from dateutil import parser

from bookings.models import Booking
from bookings.serializers import BookingSerializer

from authentication.models import GroomlyUser
from authentication.custom_permissions import ClientPermission

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

get_diary_month_param = openapi.Parameter(
    'month', openapi.IN_QUERY, description="specify the month.", type=openapi.TYPE_INTEGER)

get_diary_year_param = openapi.Parameter(
    'year', openapi.IN_QUERY, description="specify the year.", type=openapi.TYPE_INTEGER)

get_availability_date_param = openapi.Parameter(
    'date', openapi.IN_QUERY, description="specify the date.", type=openapi.TYPE_STRING, format=openapi.FORMAT_DATE)

get_client_id_param = openapi.Parameter(
    'clientID', openapi.IN_QUERY, description="user ID of client", type=openapi.TYPE_INTEGER)


# get diary
class DiaryView(APIView):
    """
    get:
    Returns a list of all appointments for the authenticated user in a given month and year.
    """

    def get_serializer(self):
        return BookingSerializer(many=True)

    @swagger_auto_schema(manual_parameters=[get_diary_month_param, get_diary_year_param])
    def get(self, request):
        try:
            month = int(request.GET['month'])
            if month < 1 or month > 12:
                raise Exception()
            year = int(request.GET['year'])
            user = request.user
            if user.is_client:
                bookings = Booking.objects.filter(
                    client=user, date__year=year, date__month=month)
            else:
                bookings = Booking.objects.filter(
                    customer=user, date__year=year, date__month=month)
            serialiser = BookingSerializer(bookings, many=True)
            return Response(serialiser.data)
        except Exception as e:
            return Response("Bad request!", status=status.HTTP_400_BAD_REQUEST)


# availability
class AvailabilityView(APIView):

    """
    post:
    Updates/Creates the availability of the authenticated client on the given day.
    get:
    Returns the availability of the client for the given day.
    """

    def get_serializer(self):
        return AvailableSerializer()

    def check_permissions(self, request):
        if request.method == 'GET':
            permission = permissions.IsAuthenticated()
        else:
            permission = ClientPermission()
        has_permissions = permission.has_permission(request, self)
        if not has_permissions:
            raise PermissionDenied

    def post(self, request, format='json'):
        input_data = request.data.copy()
        input_data['clientID'] = request.user.id
        try:
            input_data['date'] = parser.parse(
                input_data['date'], ignoretz=True, dayfirst=True).date()
        except:
            return Response("Bad request!", status=status.HTTP_400_BAD_REQUEST)
        try:
            availability = Availability.objects.get(
                client=request.user, date=input_data['date'])
        except:
            availability = None
        serializer = AvailableSerializer(
            data=input_data) if availability is None else AvailableSerializer(availability, data=input_data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(manual_parameters=[get_availability_date_param, get_client_id_param])
    def get(self, request):
        try:
            clientID = int(request.GET['clientID'])
            date = parser.parse(
                request.GET['date'], ignoretz=True, dayfirst=True).date()
            client = GroomlyUser.objects.get(id=clientID)
            availability = Availability.objects.get(client=client, date=date)
            serialiser = AvailableSerializer(availability)
            return Response(serialiser.data)
        except:
            return Response("Bad request!", status=status.HTTP_400_BAD_REQUEST)
