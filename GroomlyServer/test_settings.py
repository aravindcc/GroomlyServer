from .settings import *

SIMPLE_JWT['ACCESS_TOKEN_LIFETIME'] = timedelta(seconds=5)
SIMPLE_JWT['REFRESH_TOKEN_LIFETIME'] = timedelta(seconds=10)
