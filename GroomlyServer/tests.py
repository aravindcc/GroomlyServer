from django.test import LiveServerTestCase, tag
from bookings.tests import setup_default, default_plus
from rest_framework_simplejwt.tokens import api_settings
import subprocess
import os

from bookings.models import Booking
from authentication.models import GroomlyUser


def run_iOS_test(self, plus):
    out = open("tmpout.txt", "w")
    err = open("tmperr.txt", "w")
    command = ("xcodebuild test "
               "-workspace ../GroomlyPlatform/Groomly.xcworkspace/ "
               "-scheme Groomly "
               "-destination 'platform=iOS Simulator,name=iPhone SE (2nd generation),OS=latest' "
               f"TEST_HOST_URL={self.live_server_url} "
               f"{plus}"
               )

    process = subprocess.Popen(command, shell=True, stdout=out, stderr=err)
    process.wait()
    self.assertEqual(process.returncode, 0)
    # os.remove("tmpout.txt")
    # os.remove("tmperr.txt")


@tag('iOS', 'jwt')
class iOSJWTTests(LiveServerTestCase):
    def setUp(self):
        setup_default(self)

    def test_jwt(self):
        run_iOS_test(
            self, "-only-testing:GroomlyTests/NetworkTests/testLoginLogout")


@tag('iOS')
class iOSIntegrationTests(LiveServerTestCase):
    def setUp(self):
        setup_default(self)
        default_plus(self)
        default_plus(self, shift=2)

    def test_methods(self):
        run_iOS_test(
            self, "-skip-testing:GroomlyTests/NetworkTests/testLoginLogout")
