from django.test import TestCase
from rest_framework.test import APIClient
from django.contrib.auth.hashers import make_password

from .models import Booking, BookingStatus
from data.models import Service, ServiceCategory
from authentication.models import GroomlyUser

from datetime import datetime, timedelta


def format_timedelta(delta):
    return format_seconds(delta.seconds)


def format_seconds(s):
    hours, remainder = divmod(s, 3600)
    minutes, seconds = divmod(remainder, 60)
    return '{:02}:{:02}:{:02}'.format(int(hours), int(minutes), int(seconds))


def setup_default(self):
    client_info = {
        "email": "fake@gmail.com",
        "address": 'Robinson College',
        "city": 'Cambridge',
        "postal_code": 'CB39AN',
        "password": make_password("strongpword"),
        "formatted_address": "Grange Rd, Cambridge CB3 9AN, UK",
        "latitude": 52.2049386,
        "longitude": 0.1042334,
        "is_client": True,
    }
    self.client = GroomlyUser.objects.create(**client_info)
    customer_info = {
        "email": "fake2@gmail.com",
        "address": 'Clare College',
        "city": 'Cambridge',
        "postal_code": 'CB21TL',
        "password": make_password("strongpword"),
        "formatted_address": "Trinity Ln, Cambridge CB2 1TL, UK",
        "latitude": 52.206380,
        "longitude": 0.117180,
        "is_client": False,
    }
    self.customer = GroomlyUser.objects.create(**customer_info)
    services = []
    for (i, name) in enumerate(["one", "two", "three"]):
        for category in ServiceCategory:
            service = Service.objects.create(
                name=name,
                description="random desc",
                image_path="random path",
                key=(f'{category}{i}'),
                category=category
            )
            services.append(service)
    self.services = services


def create_new_user(is_client, plus=0):
    new_info = {
        "email": f"fake{plus + 3}@gmail.com",
        "address": 'Clare College',
        "city": 'Cambridge',
        "postal_code": 'CB21TL',
        "password": make_password("strongpword"),
        "formatted_address": "Trinity Ln, Cambridge CB2 1TL, UK",
        "latitude": 52.206380,
        "longitude": 0.117180,
        "is_client": is_client,
    }
    new_user = GroomlyUser.objects.create(**new_info)
    return new_user


def default_plus(self,
                 field=None,
                 val=None,
                 expecting=None,
                 times=None,
                 initial_services=None,
                 fails=False,
                 user=None,
                 clientID=None,
                 shift=1):
    start_time = "10:00" if times is None else times[0]
    end_time = "11:00" if times is None else times[1]
    date_str = (datetime.now() + timedelta(days=shift)
                ).date().strftime("%d-%m-%Y")
    services = [] if initial_services is None else initial_services
    if initial_services == None:
        for (i, service) in enumerate(self.services):
            services.append({
                "id": service.key,
                "price": i * 2 + 1,
                "duration": format_seconds(i * 50 + 900),
            })
    booking = {
        "clientID": (self.client.id if clientID is None else clientID),
        "date": date_str,
        "location": self.customer.formatted_address,
        "start_time": start_time,
        "end_time": end_time,
        "services": services,
    }

    if field is not None:
        booking[field] = val

    client = APIClient()
    client.force_authenticate(
        user=(self.customer if user is None else user))
    response = client.post('/booking/', booking, format='json')

    if fails:
        self.assertEqual(response.status_code, 400)
    else:
        self.assertEqual(response.status_code, 201)
        self.assertIn("id", response.data)

        if expecting is not None:
            if callable(expecting):
                expecting(response.data)
            else:
                try:
                    if response.data[field] != expecting:
                        raise Exception()
                except:
                    self.fail(f'{field} in response data not as expected.')
    return client


class BookingCreateTestCase(TestCase):

    def setUp(self):
        setup_default(self)

    def test_create_booking(self):
        default_plus(self)

    def test_create_forcing_customer_id_fails(self):
        default_plus(self, "customerID", 5, self.customer.id)

    def test_create_forcing_reject_reason_fails(self):
        default_plus(self, "reject_reason", "hello", None)

    def test_create_forcing_status_fails(self):
        def check_booking(data):
            booking = Booking.objects.get(id=int(data['id']))
            self.assertEqual(booking.status, BookingStatus.REQUESTED,
                             "Initial Booking Status not correct")
        default_plus(self,
                     "status", BookingStatus.CONFIRMED, check_booking)

    def test_create_invalid_services(self):
        default_plus(self, initial_services=[
            {
                "id": "fake",
                "price": 1,
                "duration": format_seconds(900),
            }
        ], fails=True)

    def test_create_invalid_duration(self):
        default_plus(self, initial_services=[
            {
                "id": "fake",
                "price": 1,
                "duration": format_seconds(10),
            }
        ], fails=True)

    def test_create_invalid_price(self):
        default_plus(self, initial_services=[
            {
                "id": "fake",
                "price": -1,
                "duration": format_seconds(900),
            }
        ], fails=True)

    def test_create_invalid_times(self):
        default_plus(self, times=["11:00", "10:00"], fails=True)

    def test_create_clashing_times(self):
        default_plus(self)
        default_plus(self, fails=True)

    def test_create_invalid_locations(self):
        default_plus(self,
                     "location", "33 Loyds Close, Abingdon, UK", fails=True)

    def test_create_client_fails(self):
        default_plus(self, user=self.client, fails=True)

    def test_create_empty_order_fails(self):
        default_plus(self, initial_services=[], fails=True)

    def test_create_valid_client_id(self):
        default_plus(self, clientID=10, fails=True)

    def test_get_booking(self):
        c = default_plus(self)
        c.force_authenticate(self.client)
        response = c.get('/booking/?id=1')
        self.assertEqual(response.status_code, 200)
        self.assertIn("services", response.data)
        self.assertIn("id", response.data)
        self.assertEqual(len(response.data["services"]), len(self.services))
        self.assertIn("id", response.data["services"][0])
        self.assertIn("price", response.data["services"][0])
        self.assertIn("duration", response.data["services"][0])

    def test_cannot_get_unauthed_booking(self):
        c = default_plus(self)
        new_user = create_new_user(False)
        c.force_authenticate(new_user)
        response = c.get('/booking/?id=1')
        self.assertEqual(response.status_code, 400)


class BookingUpdateTestCase(TestCase):

    def setUp(self):
        setup_default(self)
        self.c = default_plus(self)

    def try_update(self, c, update, fails, test_equality=True):
        update["id"] = 1
        response = c.post('/booking/', update, format='json')
        if fails:
            self.assertEqual(response.status_code, 400)
        else:
            self.assertEqual(response.status_code, 201)
            if test_equality:
                for (key, val) in update.items():
                    if key in response.data:
                        self.assertIn(str(val), str(response.data[key]))

    def try_update_single(self, c, field, val, fails, test_equality=True):
        update = {
            field: val
        }
        self.try_update(c, update, fails, test_equality)

    def test_time(self, client=False, in_c=None, fails=False, over_customer=None):
        c = self.c if in_c is None else in_c
        c.force_authenticate(
            self.customer if over_customer is None else over_customer)
        start_time = "12:00"
        end_time = "13:00"
        date_str = (datetime.now() + timedelta(days=5)
                    ).date().strftime("%d-%m-%Y")
        update = {
            'date': date_str,
            'start_time': start_time,
            'end_time': end_time,
        }
        if client:
            c.force_authenticate(self.client)
        self.try_update(c, update, client or fails)

        if not fails:
            booking = Booking.objects.get(id=1)
            self.assertEqual(booking.status, BookingStatus.REQUESTED)
            return c

    def test_invalid_time(self):
        c = self.c
        date_str = (datetime.now() + timedelta(days=-1)
                    ).date().strftime("%d-%m-%Y")
        update = {
            'date': date_str,
        }
        self.try_update(c, update, True)

    def test_single_invalid_time_update(self):
        c = self.c
        self.try_update_single(c, "start_time", "18:00", True)

    def test_invalid_customer_actions(self):
        c = self.c
        self.try_update_single(c, "location", "Gibson Close", True)
        self.try_update_single(c, "reject_reason", "Rejected!", True)
        for state in BookingStatus:
            self.try_update_single(c, "status", state, True)

    def test_invalid_client_actions(self):
        c = self.test_time(True)
        self.try_update_single(c, "location", "Gibson Close", True)
        self.try_update_single(c, "reject_reason", "Rejected!", True)
        invalid_states = [
            BookingStatus.COMPLETED,
            BookingStatus.CANCELLED,
            BookingStatus.REQUESTED,
            BookingStatus.RESOLVING,
        ]
        for state in invalid_states:
            self.try_update_single(c, "status", state, True)

    def test_client_accept(self, in_c=None, over_client=None):
        c = self.c if in_c is None else in_c
        c.force_authenticate(
            self.client if over_client is None else over_client)

        if over_client is None:
            self.try_update_single(c, "status", BookingStatus.CONFIRMED, False)
            booking = Booking.objects.get(id=1)
            self.assertEqual(booking.status, BookingStatus.CONFIRMED)
            return c
        else:
            self.try_update_single(c, "status", BookingStatus.CONFIRMED, True)

    def test_other_client_cannot_accept(self):
        new_user = create_new_user(True)
        self.test_client_accept(over_client=new_user)

    def test_back_and_froth(self):
        c = self.test_client_accept()
        self.test_time(in_c=c)
        self.test_client_accept()

    def test_client_rejects(self):
        c = self.c
        c.force_authenticate(self.client)
        update = {
            "reject_reason": "not available anymore",
            "status": BookingStatus.RESOLVING
        }
        self.try_update(c, update, False)

    def test_change_time_twice_fails(self):
        c = self.test_time()
        self.test_time(in_c=c, fails=True)

    def test_other_customer_cannot_update(self):
        new_user = create_new_user(False)
        self.test_time(fails=True, over_customer=new_user)


class BookingCancelTestCase(TestCase):

    def setUp(self):
        setup_default(self)
        self.c = default_plus(self)

    def delete_booking(self, c, fails, num=1):
        response = c.delete(f'/booking/?id={num}')
        if fails:
            self.assertEqual(response.status_code, 400)
        else:
            self.assertEqual(response.status_code, 201)

    def test_complete_fails(self):
        c = self.c
        booking = Booking.objects.get(id=1)
        booking.status = BookingStatus.COMPLETED
        booking.save()
        self.delete_booking(c, True)
        c.force_authenticate(self.client)
        self.delete_booking(c, True)

    def test_cancel_works(self):
        c = self.c
        self.delete_booking(c, False)
        c = default_plus(self)
        c.force_authenticate(self.client)
        self.delete_booking(c, False, num=2)
        self.assertEqual(len(Booking.objects.all()), 0)
