from django.shortcuts import render
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import PermissionDenied

from .serializers import BookingSerializer
from .models import Booking, BookingStatus

from authentication.custom_permissions import ClientPermission

from dateutil import parser

from datetime import datetime, date, timedelta


from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

get_booking_id_param = openapi.Parameter(
    'id', openapi.IN_QUERY, description="id of Booking object", type=openapi.TYPE_INTEGER)


def check_booking_finished(booking):
    now = datetime.now()
    booking_end = datetime.combine(booking.date, booking.end_time)
    if booking_end <= now:
        booking.status = BookingStatus.COMPLETED
        booking.save()


class BookingView(APIView):

    '''
        post:
        Create a booking, will perform time, location validation.
        get:
        Get the information for a booking
        delete:
        Cancel a booking.
    '''

    def get_serializer(self):
        return BookingSerializer()

    # TODO: Custom charging logic on emergency appointment and general additional charging logic
    def post(self, request, format='json'):
        input_data = request.data.copy()
        error_response = Response(
            "Invalid Request!", status=status.HTTP_400_BAD_REQUEST)
        serializer = None

        # sanitize date
        if 'date' in input_data:
            try:
                input_data['date'] = parser.parse(
                    input_data['date'], ignoretz=True, dayfirst=True).date()
            except:
                return error_response

        if "id" not in input_data:
            if request.user.is_client:
                return Response("Bad request!", status=status.HTTP_400_BAD_REQUEST)
            else:
                # create booking
                input_data['customerID'] = request.user.id
                serializer = BookingSerializer(data=input_data)
        else:
            # check valid booking and permissions
            try:
                booking_id = input_data.pop("id")
                instance = Booking.objects.get(id=int(booking_id))
                if not (instance.client == request.user or instance.customer == request.user):
                    raise Exception()
            except:
                return error_response

            check_booking_finished(instance)

            if request.user.is_client:
                only_keys = ['reject_reason', 'status']
                must_keys = ['status']
            else:
                only_keys = ['start_time', 'end_time', 'date']
                must_keys = []

            try:
                given_keys = list(input_data.keys())
                extra = len([x for x in given_keys if x not in only_keys])
                must = len([x for x in given_keys if x in must_keys])
                if extra > 0 or must != len(must_keys):
                    raise Exception()
            except:
                return error_response
            serializer = BookingSerializer(
                instance, data=input_data, partial=True)

        if serializer.is_valid():
            booking = serializer.save()
            if booking:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(manual_parameters=[get_booking_id_param])
    def delete(self, request):
        try:
            booking_id = request.GET["id"]
            instance = Booking.objects.get(id=int(booking_id))
            check_booking_finished(instance)
            if not (instance.client == request.user or instance.customer == request.user):
                raise Exception("unauthorised")
            if instance.status == BookingStatus.COMPLETED:
                raise Exception("completed appointment cannot be deleted.")
            else:
                # add custom logic for extra charging etc ...
                instance.delete()
                return Response({"state": "Appointment Deleted"}, status=status.HTTP_201_CREATED)
        except:
            return Response("Invalid Request!", status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(manual_parameters=[get_booking_id_param])
    def get(self, request):
        try:
            booking_id = request.GET["id"]
            instance = Booking.objects.get(id=int(booking_id))
            check_booking_finished(instance)
            if not (instance.client == request.user or instance.customer == request.user):
                raise Exception()
            serialiser = BookingSerializer(instance)
            return Response(serialiser.data)
        except:
            return Response(
                "Invalid Request!", status=status.HTTP_400_BAD_REQUEST)
