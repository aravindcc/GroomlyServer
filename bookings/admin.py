from django.contrib import admin
from .models import Booking, FrozenServicePrice


class BookingAdmin(admin.ModelAdmin):
    model = Booking


class FrozenServicePriceAdmin(admin.ModelAdmin):
    model = FrozenServicePrice


admin.site.register(FrozenServicePrice, FrozenServicePriceAdmin)
admin.site.register(Booking, BookingAdmin)
