from rest_framework import serializers
from dateutil import parser
from datetime import datetime, date, timedelta

from data.models import Service

from diary.models import Availability, AvailableBlocks

from authentication.models import GroomlyUser
from authentication.geotools import check_max_distance, driving_distance_points, validate_address
from authentication.serializers import ServicePriceSerializer

from .models import Booking, BookingStatus, FrozenServicePrice

import json


def check_min_elapsed(date, start, end, allow_past=False):
    if start > end:
        raise serializers.ValidationError("Invalid appointment time.")
    start_datetime = datetime.combine(date, start)
    if not allow_past and start_datetime < datetime.now():
        raise serializers.ValidationError("Invalid appointment time.")

    elapsed = datetime.combine(date, end) - start_datetime
    if elapsed < timedelta(minutes=15):
        raise serializers.ValidationError("Appointment too short.")


class FrozenServicePriceSerializer(ServicePriceSerializer):
    class Meta(ServicePriceSerializer.Meta):
        model = FrozenServicePrice
        fields = ServicePriceSerializer.Meta.fields


class BookingSerializer(serializers.ModelSerializer):
    clientID = serializers.IntegerField(source='client.id')
    customerID = serializers.IntegerField(source='customer.id')
    services = FrozenServicePriceSerializer(
        source='frozenserviceprice_set', many=True)
    date = serializers.DateField(format="%d-%m-%Y")
    start_time = serializers.TimeField()
    end_time = serializers.TimeField()
    location = serializers.CharField(max_length=200)
    reject_reason = serializers.CharField(
        max_length=200, allow_blank=True, required=False)
    status = serializers.ChoiceField(BookingStatus, required=False)

    class Meta:
        model = Booking
        fields = ('id', 'status', 'clientID', 'customerID', 'date', 'start_time', 'end_time',
                  'location', 'services', 'reject_reason')
        read_only_fields = ('id', 'status')

    def validate_data(self, data, instance=None):
        if instance is None:
            data['client'] = self.get_user(
                data.pop('client').pop('id'), True)
            data['customer'] = self.get_user(
                data.pop('customer').pop('id'), False)
            data["location"] = self.get_proximal_location(
                data['client'], data["location"])

        check_min_elapsed(
            data['date'], data['start_time'], data['end_time'])
        self.check_clashes(data, instance)

    def check_clashes(self, data, instance=None):
        # get availability blocks of client on day
        try:
            availability = Availability.objects.get(
                client=data['client'], date=data['date'])
            blocks = AvailableBlocks.objects.filter(service=availability)
            test_blocks = map(lambda x: (x.start, x.end), blocks)
            if check_clash(data['start_time'], data['end_time'], test_blocks):
                raise serializers.ValidationError(
                    "client is unavailable at that time.")
        except serializers.ValidationError as e:
            raise e
        except:
            pass

        # check bookings of customer on day
        try:
            previous_bookings = Booking.objects.filter(
                customer=data['customer'], date=data['date'])
            test_blocks = map(lambda x: (
                x.start_time, x.end_time), previous_bookings)
            if instance is not None and instance in test_blocks:
                test_blocks.remove(instance)
            if self.check_clash(data['start_time'], data['end_time'], test_blocks):
                raise serializers.ValidationError(
                    "customer has a clashing appointment.")
        except serializers.ValidationError as e:
            raise e

        # check bookings of client on day
        try:
            previous_bookings = Booking.objects.filter(
                client=data['client'], date=data['date'])
            test_blocks = map(lambda x: (
                x.start_time, x.end_time), previous_bookings)
            if instance is not None and instance in test_blocks:
                test_blocks.remove(instance)
            if self.check_clash(data['start_time'], data['end_time'], test_blocks):
                raise serializers.ValidationError(
                    "client has a clashing appointment.")
        except serializers.ValidationError as e:
            raise e

    # validate time
    def check_clash(self, start, end, blocks):
        for (block_start, block_end) in blocks:
            if block_start <= start <= block_end:
                return True
            elif block_start <= end <= block_end:
                return True
        return False

    # validate location
    def get_proximal_location(self, client, location):
        latlng, address = validate_address(location)
        origin = (client.latitude, client.longitude)
        distance = driving_distance_points(origin, tuple(latlng))
        check_max_distance(distance)
        return address

    def get_user(self, id, be_client):
        try:
            user = GroomlyUser.objects.get(id=id)
            if be_client and not user.is_client:
                raise Exception()
            return user
        except:
            raise serializers.ValidationError("invalid customer/client.")

    def create_order(self, instance, service_data):
        if len(service_data) == 0:
            raise serializers.ValidationError('no services requested')

        services = []
        for service_obj in service_data:
            try:
                key = service_obj.pop("service").pop("key")
                price = service_obj.pop("price")
                duration = service_obj.pop("duration")
                tb = Service.objects.get(key=key)
                services.append((tb, price, duration))
            except Exception as e:
                raise serializers.ValidationError('invalid service.')

        instance.save()
        for (tb, price, duration) in services:
            tbs = FrozenServicePrice.objects.create(
                booking=instance, service=tb, price=price, duration=duration)
            tbs.save()
        return instance

    def create(self, validated_data):
        self.validate_data(validated_data)
        validated_data['status'] = BookingStatus.REQUESTED
        validated_data["reject_reason"] = None
        service_data = validated_data.pop("frozenserviceprice_set")
        instance = Booking(**validated_data)
        return self.create_order(instance, service_data)

    # Minor state change logic double check!
    def update(self, instance, validated_data):
        def get(key):
            if key in validated_data:
                return validated_data[key]
            else:
                return getattr(instance, key)

        if "status" in validated_data:  # use this to differentiate between client and customer
            if instance.status == BookingStatus.REQUESTED:
                status = int(validated_data["status"])
                if status == BookingStatus.CONFIRMED:
                    instance.status = BookingStatus.CONFIRMED
                    instance.reject_reason = None
                    instance.save()
                elif status == BookingStatus.RESOLVING:
                    try:
                        reason = validated_data["reject_reason"]
                        if reason == "":
                            raise Excepton()
                    except:
                        raise serializers.ValidationError("invalid reason.")
                    instance.status = BookingStatus.RESOLVING
                    instance.reject_reason = reason
                    instance.save()
                else:
                    raise serializers.ValidationError("invalid status change.")
            else:
                raise serializers.ValidationError("invalid status change.")
        else:
            if instance.status == BookingStatus.COMPLETED or instance.status == BookingStatus.CANCELLED:
                raise serializers.ValidationError("invalid status change.")

            # validate new times
            data = {
                "start_time": get("start_time"),
                "end_time": get("end_time"),
                "date": get("date"),
                "client": instance.client,
                "customer": instance.customer,
            }
            self.validate_data(data, instance)

            if (instance.date == data["date"] and
                instance.start_time == data["start_time"] and
                    instance.end_time == data["end_time"]):
                raise serializers.ValidationError(
                    "same time, no update required.")

            instance.date = data["date"]
            instance.start_time = data["start_time"]
            instance.end_time = data["end_time"]
            instance.status = BookingStatus.REQUESTED
            instance.save()
        return instance
