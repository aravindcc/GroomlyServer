from django.db import models
from authentication.models import GroomlyUser
from data.models import Service


class BookingStatus(models.IntegerChoices):
    REQUESTED = 1
    CONFIRMED = 2
    COMPLETED = 3
    RESOLVING = 4
    CANCELLED = 5


class Booking(models.Model):
    status = models.IntegerField(choices=BookingStatus.choices)
    client = models.ForeignKey(
        GroomlyUser,
        on_delete=models.CASCADE,
        limit_choices_to={'is_client': True},
        related_name='+',
    )
    customer = models.ForeignKey(
        GroomlyUser,
        on_delete=models.CASCADE,
        related_name='+',
    )
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    location = models.CharField(max_length=200)
    reject_reason = models.CharField(max_length=200, blank=True, null=True)
    services = models.ManyToManyField(
        Service,
        through='FrozenServicePrice',
    )


class FrozenServicePrice(models.Model):
    booking = models.ForeignKey(
        Booking, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    price = models.FloatField()
    duration = models.DurationField()
